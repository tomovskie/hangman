package localDB;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.content.Context;

import com.rvir.project.hangman.model.Category;
import com.rvir.project.hangman.model.Game;
import com.rvir.project.hangman.model.User;
import com.rvir.project.hangman.model.Word;

@Database(entities = {User.class, Category.class, Word.class, Game.class}, version = 1, exportSchema = false)
public abstract  class DataBase extends RoomDatabase {

    private  static DataBase INSTANCE;

    public abstract DaoAccess daoAccess();

    private static final  Object sLock = new Object();

    public static DataBase getInstance(Context context){
        synchronized (sLock){
            if(INSTANCE==null){
                INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                        DataBase.class,"DAtabase.db").allowMainThreadQueries().build();
            }
            return  INSTANCE;
        }
    }

}
