package localDB;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rvir.project.hangman.model.Category;
import com.rvir.project.hangman.model.Game;
import com.rvir.project.hangman.model.User;
import com.rvir.project.hangman.model.Word;

import java.util.List;

@Dao
public interface DaoAccess {

    //Za avtentifikacija na korisnicko ime i pasvord
    /*@Query("select * from User where name=:username and  + password=:userPassowrd")
    List<User> checkUser(String username,String userPassowrd);*/

    //Za proveruvanje dali postoi korisnik so takvo korisnicko ime
    @Query("select * from User where name=:username")
    List<User> checkIfUsernameExists(String username);

    //Za proveruvanje dali posti registriran korisnik sto go koristi toj meil
    /*@Query("select * from User where email=:email")
    List<User> checkIfEmailExists(String email);*/

    //Za vnesuvanje nov korisnik vo podatocnata baza
   /* @Insert(onConflict = OnConflictStrategy.REPLACE)
    void register(User user);*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void userInsert(User user);

    //Vrakja id na posledniot vnesen korisnik vo podatocnata baza
    /*@Query("SELECT id FROM user ORDER BY id DESC LIMIT 1")
    int getLastInserted();*/

    @Query("SELECT * FROM User where id=:id and name=:name")
    List<User> checkUser(int id,String name);

    //Gi vrakja site korisnici od user table
    @Query("select * from User")
    List<User> getAllUsers();

    @Query("delete from User")
    void deleteAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void categorySync(Category category);

    @Query("select * from Category")
    List<Category> getCategories();

    @Query("delete from Category")
    void deleteCategories();

    @Query("select * from Word")
    List<Word> getWords();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void wordSync(Word word);

    @Query("select Word.name from Word,Category where Category.id=Word.id_category and Category.name=:category ")
    List<String>getWordsName(String category);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addEndGameResult(Game game);

    @Query("select * from Game")
    List<Game> getAllFromGame();

    @Query("delete from Game")
    void deleteAllGames();

    @Query("Select * from Game where isSync=:isSync")
    List<Game> syncGame(int isSync);

    @Query("update Game set isSync=1 where isSync=0")
    void updateLocaly();

    @Query("delete from word")
    void deleteWords();

    @Query("select * from game where userId=:userId order by level desc,date desc")
    List<Game> getGetUserResults(int userId);

}
