package com.rvir.project.hangman;

public class Constants {

    private static final String ROOT_URL = "http://hm-r-v-i-r.000webhostapp.com/";

    public static final String URL_REGISTER = ROOT_URL+"registration.php";
    public static final String URL_LOGIN = ROOT_URL+"login.php";
    public static final String URL_ChooseCategory = ROOT_URL+"chooseCategory.php";
    public static final String URL_SaveScore = ROOT_URL+"saveResults.php";
    public static final String URL_SyncCategory= ROOT_URL+"syncCategory.php";
    public static final String URL_SyncWord= ROOT_URL+"syncWord.php";
    public static final String URL_SyncGame= ROOT_URL+"syncGame.php";
    public static final String URL_UserResults= ROOT_URL+"userResults.php";
    public static final String URL_HighScore= ROOT_URL+"highScore.php";

}
