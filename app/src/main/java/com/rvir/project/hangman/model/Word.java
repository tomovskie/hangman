package com.rvir.project.hangman.model;


import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = @ForeignKey(entity = Category.class,
        parentColumns = "id",
        childColumns = "id_category",
        onDelete = ForeignKey.CASCADE))
public class Word {

    @PrimaryKey(autoGenerate = true)
    private  int id;
    private String name;
    private int id_category;

    public Word(int id, String name, int id_category) {
        this.id = id;
        this.name = name;
        this.id_category = id_category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_category() {
        return id_category;
    }

    public void setId_cateory(int id_cateory) {
        this.id_category = id_cateory;
    }
}
