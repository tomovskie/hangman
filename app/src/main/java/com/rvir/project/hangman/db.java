package com.rvir.project.hangman;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/*  OVAA KLASA NE SE KORISTI  */
public class db extends AsyncTask<String, String, String> {

    Context context;
    AlertDialog alertDialog;

    db(Context ctx) {
        context = ctx;
    }

    @Override
    protected String doInBackground(String... params) {
        //Prviot parametat sto e dodaden vo execute vo login.java
        //metodata koga ke se klikne kopceto za logiranje
        String type = params[0];
        //String login_url = "http://hm-r-v-i-r.000webhostapp.com/login.php";
        String registration_url = "http://hm-r-v-i-r.000webhostapp.com/registration.php";

        if (type.equals("login")) {
            try {
                String user_name = params[1];
                String password = params[2];
                //URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = ServerConnection.getHttpURLConnection();
                //request metodata se koristi slicno kako rest request
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                //Za request
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                //Gi dodava vo url parametrite za username i password za posle vo fajlot koj sto e naserverot login.php da gi prevzeme
                String post_data = URLEncoder.encode("user_name", "UTF-8") + "=" + URLEncoder.encode(user_name, "UTF-8") + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();

                // Za response
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line;
                while ((line =bufferedReader.readLine())!=null){
                    result+=line;
                }
                System.out.println("TUKAAAAAAAAAA "+result);
                bufferedReader.close();
                inputStream.close();

                httpURLConnection.disconnect();
                return  result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(type.equals("registration")) {
            try {
                String user_name = params[1];
                String user_email = params[2];
                String user_password = params[3];
                URL url = new URL(registration_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                //request metodata se koristi slicno kako rest request
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                //Za request
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                //Gi dodava vo url parametrite za username i password za posle vo fajlot koj sto e naserverot login.php da gi prevzeme
                String post_data = URLEncoder.encode("user_name", "UTF-8") + "=" + URLEncoder.encode(user_name, "UTF-8") + "&"
                        + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(user_email, "UTF-8") + "&"
                        + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(user_password, "UTF-8");
                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();

                // Za response
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                String result="";
                String line;
                while ((line =bufferedReader.readLine())!=null){
                    result+=line;
                }

                System.out.println("TUKAAAAAAAA "+result);

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return  result;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
      //alertDialog = new AlertDialog.Builder(context).create();
      //alertDialog.setTitle("Login Status");
    }

    @Override
    protected void onPostExecute(String result) {
      //  alertDialog.setMessage(result);
      // alertDialog.show();1
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);

            JSONArray re = jsonObject.getJSONArray("result");

            Boolean error = false;

            for (int i = 0; i < re.length(); i++) {
                JSONObject jo = re.getJSONObject(i);
                error=jo.getBoolean("error");
                if(error==true) {
                   String message= jo.getString("msg");
                    Toast.makeText(context, message,
                            Toast.LENGTH_LONG).show();
                }
                else if (error==false){
                   String userName = jo.getString("name");
                   String userEmail = jo.getString("email");
                   int userId = jo.getInt("id");
                    Intent intent = new Intent(context,ChooseCategoryActivity.class);
                    context.startActivity(intent);
                    SharedPrefManager.getInstance(context).userLogin(userId, userName, userEmail);
                }
            }
        }
         catch (JSONException e) {
                e.printStackTrace();
            }

    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }
}
