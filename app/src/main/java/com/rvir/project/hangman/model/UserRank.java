package com.rvir.project.hangman.model;

public class UserRank {

    String userName;
    String level;

    public UserRank(String userName,String level){
        this.userName=userName;
        this.level=level;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
