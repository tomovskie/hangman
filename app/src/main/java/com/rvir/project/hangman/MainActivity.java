package com.rvir.project.hangman;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rvir.project.hangman.model.Category;
import com.rvir.project.hangman.model.Game;
import com.rvir.project.hangman.model.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import localDB.DataBase;

public class MainActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private NetworkStateReceiver networkStateReceiver;
    public static boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button newGameButton = (Button) findViewById(R.id.newgamebutton);
        Button settingsButton = (Button) findViewById(R.id.settingsbutton);
        Button instructionButton = (Button) findViewById(R.id.instructionbutton);
        Button aboutButton = (Button) findViewById(R.id.aboutbutton);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        // DataBase.getInstance(this).daoAccess().deleteAllGames();

       /*int userId = SharedPrefManager.getInstance(this).getuserId();

        ArrayList<Game> userGames = (ArrayList<Game>) DataBase.getInstance(this).daoAccess().getGetUserResults(userId);
        for (Game game : userGames
        ) {
            System.out.println("Level TUKAAAAAA " + game.getLevel() + " Date TUKAAAA " + game.getDate());
        }
        */

        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SharedPrefManager.getInstance(getApplicationContext()).isLoggedIn()) {
                    Intent intent = new Intent(getApplicationContext(), ChooseCategoryActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), login.class);
                    startActivity(intent);
                }
            }
        });


        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });


        instructionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), InstructionActivity.class);
                startActivity(intent);
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(intent);
            }
        });
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    // Zacuvivanje na kategorii vo lokalnata baza
    public void syncCategory() {
        System.out.println("NOVO ZA SINHRONIZIRANJE KATEGORIJA");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_SyncCategory,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        List<Category> storedCategories = DataBase.getInstance(getApplicationContext()).daoAccess().getCategories();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray re = jsonObject.getJSONArray("result");
                            int syncCounter = 0;
                            int id = 0;
                            String name;
                            for (int i = 0; i < re.length(); i++) {
                                JSONObject jo = re.getJSONObject(i);
                                id = jo.getInt("id");
                                name = jo.getString("name");
                                for (int j = 0; j < storedCategories.size(); j++) {
                                    if (storedCategories.get(j).getName().equals(name)) {
                                        syncCounter++;
                                    }
                                }
                                if (syncCounter == 0) {
                                    DataBase.getInstance(getApplicationContext()).daoAccess().categorySync(new Category(
                                            id,
                                            name
                                    ));
                                } else {
                                    System.out.println("Is synchronized");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void syncWords() {
        System.out.println("NOVO ZA SINHRONIZIRANJE ZBOROVI");
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_SyncWord,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        List<Word> storedwords = DataBase.getInstance(getApplicationContext()).daoAccess().getWords();
                        System.out.println("Stored words " + storedwords.size());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray re = jsonObject.getJSONArray("result");
                            int syncCounter = 0;
                            int id = 0;
                            String name;
                            int id_category = 0;
                            for (int i = 0; i < re.length(); i++) {
                                JSONObject jo = re.getJSONObject(i);
                                id = jo.getInt("id");
                                name = jo.getString("name");
                                id_category = jo.getInt("idCategory");
                                for (int j = 0; j < storedwords.size(); j++) {
                                    if (storedwords.get(j).getName().equals(name)) {
                                        syncCounter++;
                                    }
                                }
                                if (syncCounter == 0) {
                                    System.out.println("DODADENO " + name);
                                    DataBase.getInstance(getApplicationContext()).daoAccess().wordSync(new Word(
                                            id,
                                            name,
                                            id_category
                                    ));
                                } else {
                                    System.out.println("Is synchronized");
                                }
                                syncCounter = 0;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void syncGame() throws JSONException {
        ArrayList<Game> syncGames = (ArrayList<Game>) DataBase.getInstance(getApplicationContext()).daoAccess().syncGame(0);
        // JsonArray games =  (new Gson()).toJson(syncGames, new TypeToken<ArrayList<Game>>() {}.getType());
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < syncGames.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", syncGames.get(i).getUserId());
            jsonObject.put("level", syncGames.get(i).getLevel());
            jsonObject.put("categoryName", syncGames.get(i).getCategoryName());
            jsonObject.put("date", syncGames.get(i).getDate());
            jsonArray.put(jsonObject);
        }
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("Games", jsonArray);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_SyncGame,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //System.out.println("RESPONSE " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray re = jsonObject.getJSONArray("result");
                            Boolean error = false;
                            for (int i = 0; i < re.length(); i++) {
                                JSONObject jo = re.getJSONObject(i);
                                error = jo.getBoolean("error");
                                if (error == true) {
                                    System.out.println("IMA GRESKA" + jo.getBoolean("msg"));
                                } else {
                                    System.out.println("DOBRO E ");
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("syncGames", jsonObject.toString());
                DataBase.getInstance(getApplicationContext()).daoAccess().updateLocaly();
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);


    }

    @Override
    public void networkAvailable() {
        isConnected = true;
        syncCategory();
        syncWords();
        if (!(DataBase.getInstance(this).daoAccess().syncGame(0).isEmpty())) {
            System.out.println("IMA NOVI");
            try {
                syncGame();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Ima internet " + isConnected);
    }

    @Override
    public void networkUnavailable() {
        isConnected = false;
        System.out.println("NEMA INTERNET " + isConnected);
    }
}
