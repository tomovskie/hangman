package com.rvir.project.hangman;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rvir.project.hangman.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import localDB.DaoAccess;
import localDB.DataBase;

public class login extends AppCompatActivity {

    EditText username, passoword;
    private DaoAccess daoAccess;
    private ExecutorService executorService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username);
        passoword = (EditText) findViewById(R.id.password);

    }

    public void onLogin(View view) {
        final String usernameText = username.getText().toString();
        final String passwordText = passoword.getText().toString();
      /*  String type="login";
        db db = new db(this);                                   <-- STAROTO
        db.execute(type,usernameText,passwordText);*/

        if (isNetworkConnected()) {
            remoteDB(usernameText, passwordText);
            System.out.println("REMOTE");

        } else {
           Toast.makeText(
                   this,
                    "Please check your internet connection",
                   Toast.LENGTH_LONG);
        }

    }

    public void remoteDB(String usernameText, String passwordText) {
        final String password = passwordText;
        final String username = usernameText;

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray re = jsonObject.getJSONArray("result");
                            for (int i = 0; i < re.length(); i++) {
                                JSONObject jo = re.getJSONObject(i);
                                if (!jo.getBoolean("error")) {
                                    SharedPrefManager.getInstance(getApplicationContext())
                                            .userLogin(
                                                    jo.getInt("id"),
                                                    jo.getString("name"),
                                                    jo.getString("email")
                                            );
                                    int id = jo.getInt("id");
                                    String name=jo.getString("name");
                                    List<User> storedUsers = DataBase.getInstance(getApplicationContext()).daoAccess().checkUser(id,name);
                                    if(storedUsers.size()==0){
                                        DataBase.getInstance(getApplicationContext()).daoAccess().userInsert(new User(id,name));
                                        //System.out.println("DODADEN USER VO BAZA");
                                    }else{
                                       // System.out.println("VEKJE BIL DODADEN VO BAZA");
                                    }
                                    startActivity(new Intent(getApplicationContext(), ChooseCategoryActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            jo.getString("msg"),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(
                                getApplicationContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_name", username);
                params.put("password", password);
                return params;
            }

        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

   /* public void localDB(String usernameText, final String passwordText) {

        List<User> checkUser=null;

        if (usernameText.equals("") && passwordText.equals("")) {
            Toast.makeText(
                    getApplicationContext(),
                    "Please enter username and password",
                    Toast.LENGTH_LONG
            ).show();
        } else if (usernameText.equals("")) {
            Toast.makeText(
                    getApplicationContext(),
                    "Please enter username",
                    Toast.LENGTH_LONG
            ).show();
        } else if (passwordText.equals("")) {
            Toast.makeText(
                    getApplicationContext(),
                    "Please enter password",
                    Toast.LENGTH_LONG
            ).show();
        } else {
                checkUser = DataBase.getInstance(this).daoAccess().checkUser(
                    usernameText,
                    passwordText
            );

            if (checkUser.size() > 0) {
                int userId=checkUser.get(0).getId();
                String userMail=checkUser.get(0).getEmail();
                SharedPrefManager.getInstance(getApplicationContext()).userLogin(
                        userId,
                        usernameText,
                        userMail);
                Intent intent = new Intent(getApplicationContext(),ChooseCategoryActivity.class);
                startActivity(intent);
            }
            else{
                Toast.makeText(
                        getApplicationContext(),
                        "Incorrect username and password please enter correct",
                        Toast.LENGTH_LONG
                        ).show();
            }
        }
    }*/

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void onRegistration(View view) {
        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
        startActivity(intent);
    }

}
