package com.rvir.project.hangman;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;

public class RankFragment extends AppCompatActivity  {

    private BottomNavigationView bottomNavigationView;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ranking);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.categoryRank);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_high_score);

        ArrayList<String> categories = new ArrayList<>();
        categories.add("Fruits");
        categories.add("Vegetables");
        categories.add("Drinks");
        categories.add("Sports");
        categories.add("Countries");
        categories.add("Animals");
        categories.add("Cities");
        categories.add("Cars");

        CategoryAdapter categoryAdapter = new CategoryAdapter(this,categories);
        recyclerView.setAdapter(categoryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case  R.id.yourScore:
                        //selected = RankFragment.newInstance();
                       /* Intent intentRank = new Intent(getApplicationContext(), EndGameActivity.class);
                        startActivity(intentRank);*/
                        break;
                    case  R.id.action_newGame:
                        Hangman.coins = 0;
                        Hangman.wordCount = 0;
                        Hangman.level = 1;
                        Hangman.numOfPossibleWords = 12;
                        GameplayActivity.numberOfLives = 3;
                        Intent intent = new Intent(getApplicationContext(), ChooseCategoryActivity.class);
                        startActivity(intent);
                        break;
                }
                return  true;
            }
        });


    }


}
