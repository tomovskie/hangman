package com.rvir.project.hangman;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rvir.project.hangman.model.UserScore;

import java.util.ArrayList;

public class HighScoreAdapter extends RecyclerView.Adapter<HighScoreAdapter.ViewHolder> {

    Context context;
    ArrayList<UserScore> scores;

    public HighScoreAdapter(Context context, ArrayList<UserScore> scores){
        this.context=context;
        this.scores=scores;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.score_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
            if(i==0) {
                viewHolder.textView1.setTextColor(Color.RED);
                viewHolder.textView2.setTextColor(Color.RED);
                viewHolder.textView1.setText(Integer.toString(scores.get(i).getLevel()));
                viewHolder.textView2.setText(scores.get(i).getDate());
            }else {
                viewHolder.textView1.setText(Integer.toString(scores.get(i).getLevel()));
                viewHolder.textView2.setText(scores.get(i).getDate());
            }
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView1,textView2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView1=itemView.findViewById(R.id.levelTextView);
            textView2=itemView.findViewById(R.id.dateTextView);

        }
    }
}
