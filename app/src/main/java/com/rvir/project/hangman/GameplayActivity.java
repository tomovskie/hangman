package com.rvir.project.hangman;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import localDB.DataBase;

public class GameplayActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<String> dictionary; //Lista so zborcinja za sekoja kategorija posebni zborcinja se zacuvani
    public static ImageView game_image_view;
    public static ImageView heart_image;
    private int guessesLeft;
    public static int numberOfLives = 3;
    private TextView guessLeftBox;

    //private char guess;
    public GameplayActivity() {

    }

    //Bukvi sto moze da se vnesat preku tastatura
    public static final HashMap<Integer, Character> KEYBOARD = new HashMap<>();

    static {
        KEYBOARD.put(R.id.button1, 'a');
        KEYBOARD.put(R.id.button2, 'b');
        KEYBOARD.put(R.id.button3, 'c');
        KEYBOARD.put(R.id.button4, 'd');
        KEYBOARD.put(R.id.button5, 'e');
        KEYBOARD.put(R.id.button6, 'f');
        KEYBOARD.put(R.id.button7, 'g');
        KEYBOARD.put(R.id.button8, 'h');
        KEYBOARD.put(R.id.button9, 'i');
        KEYBOARD.put(R.id.button10, 'j');
        KEYBOARD.put(R.id.button11, 'k');
        KEYBOARD.put(R.id.button12, 'l');
        KEYBOARD.put(R.id.button13, 'm');
        KEYBOARD.put(R.id.button14, 'n');
        KEYBOARD.put(R.id.button15, 'o');
        KEYBOARD.put(R.id.button16, 'p');
        KEYBOARD.put(R.id.button17, 'q');
        KEYBOARD.put(R.id.button18, 'r');
        KEYBOARD.put(R.id.button19, 's');
        KEYBOARD.put(R.id.button20, 't');
        KEYBOARD.put(R.id.button21, 'u');
        KEYBOARD.put(R.id.button22, 'v');
        KEYBOARD.put(R.id.button23, 'w');
        KEYBOARD.put(R.id.button24, 'x');
        KEYBOARD.put(R.id.button25, 'y');
        KEYBOARD.put(R.id.button26, 'z');
    }

    //private static final int alphabets = 26;  Ne se koristi ova

    private boolean isSoundOn, isVibrateOn;
    private Hangman hangman;
    private TextView dashBox = null;
    public static TextView levelBox = null;
    public static TextView coinsBox = null;
    public static String categoryName;
    private Button nextWordBtn;
    public static Button helpLetterBtn;

    public static Button button1, button2, button4, button3, button5, button6, button7, button8, button9, button10,
            button11, button12, button13, button14, button15, button16, button17, button18, button19,
            button20, button21, button22, button23, button24, button25, button26;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameplay);
        Intent intent = getIntent();
        categoryName = intent.getStringExtra("name");

        nextWordBtn = (Button) findViewById(R.id.nextWord);
        nextWordBtn.setVisibility(View.INVISIBLE);
        nextWordBtn.setOnClickListener(this);

        helpLetterBtn = (Button) findViewById(R.id.btnHelp);
        helpLetterBtn.setVisibility(View.INVISIBLE);
        helpLetterBtn.setOnClickListener(this);

        game_image_view = (ImageView) findViewById(R.id.imageView2);
        heart_image = (ImageView) findViewById(R.id.heart_image);

        heart_image = (ImageView) findViewById(R.id.heart_image);

        SharedPreferences sharedPreferences = getSharedPreferences("MySettings", Context.MODE_PRIVATE);
        guessesLeft = 6;
        //sharedPreferences.getInt("guesses", 15);

        //wordLength = sharedPreferences.getInt("wordLength", 5);

        isSoundOn = sharedPreferences.getBoolean("sound", true);
        isVibrateOn = sharedPreferences.getBoolean("vibration", true);

        TextView categoryBox = (TextView) findViewById(R.id.statusbox);
        dashBox = (TextView) findViewById(R.id.dashbox);
        guessLeftBox = (TextView) findViewById(R.id.guessleftbox);

        levelBox = (TextView) findViewById(R.id.level);
        coinsBox = (TextView) findViewById(R.id.coins);

        //citanje na zborcinjata od datotekata dictionary.txt
        if (isNetworkConnected()) {
            makeDictionary(categoryName, dashBox, guessesLeft); // Ako e konektirano pravi dictionary od online bazata
        } else {
            makeDictionaryLocalDB(categoryName, dashBox, guessesLeft); // Ako ne e konektirano pravi dictionary od lokalnata baza
        }

        categoryBox.setText(categoryName);
        //dashBox.setText(hangman.getSecretWord(wordLength));
        //guessLeftBox.setText("Guesses Left: " + guessesLeft);

        button1 = (Button) findViewById(R.id.button1);
        button1.setEnabled(true);
        button1.setSoundEffectsEnabled(false);
        button1.setText("a");
        //button1.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button1.setOnClickListener(this);

        button2 = (Button) findViewById(R.id.button2);
        button2.setEnabled(true);
        button2.setSoundEffectsEnabled(false);
        button2.setText("b");
        //button2.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button2.setOnClickListener(this);

        button3 = (Button) findViewById(R.id.button3);
        button3.setEnabled(true);
        button3.setSoundEffectsEnabled(false);
        button3.setText("c");
        //button3.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button3.setOnClickListener(this);

        button4 = (Button) findViewById(R.id.button4);
        button4.setEnabled(true);
        button4.setSoundEffectsEnabled(false);
        button4.setText("d");
        //button4.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button4.setOnClickListener(this);

        button5 = (Button) findViewById(R.id.button5);
        button5.setEnabled(true);
        button5.setSoundEffectsEnabled(false);
        button5.setText("e");
        //button5.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button5.setOnClickListener(this);

        button6 = (Button) findViewById(R.id.button6);
        button6.setEnabled(true);
        button6.setSoundEffectsEnabled(false);
        button6.setText("f");
        //button6.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button6.setOnClickListener(this);

        button7 = (Button) findViewById(R.id.button7);
        button7.setEnabled(true);
        button7.setSoundEffectsEnabled(false);
        button7.setText("g");
        //button7.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button7.setOnClickListener(this);

        button8 = (Button) findViewById(R.id.button8);
        button8.setEnabled(true);
        button8.setSoundEffectsEnabled(false);
        button8.setText("h");
        //button8.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button8.setOnClickListener(this);

        button9 = (Button) findViewById(R.id.button9);
        button9.setEnabled(true);
        button9.setSoundEffectsEnabled(false);
        button9.setText("i");
        //button9.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button9.setOnClickListener(this);

        button10 = (Button) findViewById(R.id.button10);
        button10.setEnabled(true);
        button10.setSoundEffectsEnabled(false);
        button10.setText("j");
        //button10.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button10.setOnClickListener(this);

        button11 = (Button) findViewById(R.id.button11);
        button11.setEnabled(true);
        button11.setSoundEffectsEnabled(false);
        button11.setText("k");
        //button11.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button11.setOnClickListener(this);

        button12 = (Button) findViewById(R.id.button12);
        button12.setEnabled(true);
        button12.setSoundEffectsEnabled(false);
        button12.setText("l");
        //button12.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button12.setOnClickListener(this);

        button13 = (Button) findViewById(R.id.button13);
        button13.setEnabled(true);
        button13.setSoundEffectsEnabled(false);
        button13.setText("m");
        //button13.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button13.setOnClickListener(this);

        button14 = (Button) findViewById(R.id.button14);
        button14.setEnabled(true);
        button14.setSoundEffectsEnabled(false);
        button14.setText("n");
        //button14.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button14.setOnClickListener(this);

        button15 = (Button) findViewById(R.id.button15);
        button15.setEnabled(true);
        button15.setSoundEffectsEnabled(false);
        button15.setText("o");
        //button15.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button15.setOnClickListener(this);

        button16 = (Button) findViewById(R.id.button16);
        button16.setEnabled(true);
        button16.setSoundEffectsEnabled(false);
        button16.setText("p");
        //button16.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button16.setOnClickListener(this);

        button17 = (Button) findViewById(R.id.button17);
        button17.setEnabled(true);
        button17.setSoundEffectsEnabled(false);
        button17.setText("q");
        //button17.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button17.setOnClickListener(this);

        button18 = (Button) findViewById(R.id.button18);
        button18.setEnabled(true);
        button18.setSoundEffectsEnabled(false);
        button18.setText("r");
        //button18.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button18.setOnClickListener(this);

        button19 = (Button) findViewById(R.id.button19);
        button19.setEnabled(true);
        button19.setSoundEffectsEnabled(false);
        button19.setText("s");
        //button19.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button19.setOnClickListener(this);

        button20 = (Button) findViewById(R.id.button20);
        button20.setEnabled(true);
        button20.setSoundEffectsEnabled(false);
        button20.setText("t");
        //button20.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button20.setOnClickListener(this);

        button21 = (Button) findViewById(R.id.button21);
        button21.setEnabled(true);
        button21.setSoundEffectsEnabled(false);
        button21.setText("u");
        //button21.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button21.setOnClickListener(this);

        button22 = (Button) findViewById(R.id.button22);
        button22.setEnabled(true);
        button22.setSoundEffectsEnabled(false);
        button22.setText("v");
        //button22.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button22.setOnClickListener(this);

        button23 = (Button) findViewById(R.id.button23);
        button23.setEnabled(true);
        button23.setSoundEffectsEnabled(false);
        button23.setText("w");
       // button23.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button23.setOnClickListener(this);

        button24 = (Button) findViewById(R.id.button24);
        button24.setEnabled(true);
        button24.setSoundEffectsEnabled(false);
        button24.setText("x");
        //button24.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button24.setOnClickListener(this);

        button25 = (Button) findViewById(R.id.button25);
        button25.setEnabled(true);
        button25.setSoundEffectsEnabled(false);
        button25.setText("y");
        //button25.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button25.setOnClickListener(this);

        button26 = (Button) findViewById(R.id.button26);
        button26.setEnabled(true);
        button26.setSoundEffectsEnabled(false);
        button26.setText("z");
        //button26.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        button26.setOnClickListener(this);

    }

    // Kreiranje na menito
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gameplay, menu);
        return true;
    }

    //Ako e kliknato za how to play ili logout sto da se otvori (Tuka da promenam za setings namesto google)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_HowToPlay) {
            Intent intent = new Intent(getApplicationContext(), InstructionActivity.class);
            startActivity(intent);
            return true;
        } else if (item.getItemId() == R.id.action_logOut) {
            SharedPrefManager.getInstance(this).logout();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.action_HighScore){
            Intent intent = new Intent(getApplicationContext(), HighScoreActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    //Se kreira dictionary so zborovi.
    public void makeDictionary(String category, TextView dashBox, int guessesLeft) {
        dictionary = new ArrayList<>();
        //final int wordLengthText = wordLength;
        final int guessesLeftText = guessesLeft;
        final TextView dashBoxH = dashBox;
        final String cateogryText = category;
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_ChooseCategory,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArrayResult = jsonObject.getJSONArray("result");
                            JSONArray jsonArrayWords = jsonObject.getJSONArray("words");
                            JSONObject jo = jsonArrayResult.getJSONObject(0);
                            if (!jo.getBoolean("error")) {
                                for (int i = 0; i < jsonArrayWords.length(); i++) {
                                    System.out.println("Word " + jsonArrayWords.get(i).toString().trim());
                                    dictionary.add(jsonArrayWords.get(i).toString().trim());
                                }
                            } else {
                                jo = jsonArrayResult.getJSONObject(1);
                                Toast.makeText(
                                        getApplicationContext(),
                                        jo.getString("msg"),
                                        Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hangman = new Hangman(
                                dictionary,
                                guessesLeftText);
                        dashBoxH.setText(hangman.getSecretWord(hangman.randomWord.length()));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(
                                getApplicationContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("category_name", cateogryText);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void makeDictionaryLocalDB(String category, TextView dashBox, int guessesLeft) {
        System.out.println("LOKALNO");
        dictionary = new ArrayList<>();
        dictionary = (ArrayList<String>) DataBase.getInstance(getApplicationContext()).daoAccess().getWordsName(category);
        hangman = new Hangman(
                dictionary,
                guessesLeft);
        dashBox.setText(hangman.getSecretWord(hangman.randomWord.length()));
    }

    //So sekoe klikanje na kopceto se povikuva
    public void updateGame(char guessedLetter, View view) {

        //Ako se pusteni vibracii da vibrira koga ke klikne na nekoe kopce
        //System.out.println("Checked word "+hangman.checkNewWord);

        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (isVibrateOn) {
            vibe.vibrate(50);
        }

        MediaPlayer wrongAnswer = MediaPlayer.create(this, R.raw.buzzer);
        MediaPlayer rightAnswer = MediaPlayer.create(this, R.raw.correct);
        MediaPlayer surprised = MediaPlayer.create(this, R.raw.surprised);

        TextView statusBox = (TextView) findViewById(R.id.statusbox);
        dashBox = (TextView) findViewById(R.id.dashbox);
        //guessLeftBox = (TextView) findViewById(R.id.guessleftbox);

        //Vrakja true ako bukvata se sodrzi vo zborot,ako ne se sodrzi vo zorot vrakja false
        boolean bool = hangman.guessMade(guessedLetter, dashBox, this);

        changeIconLevel(hangman.level, hangman.guessesLeft);
       // level1(hangman.guessesLeft);

        String status;
        String secretWord = hangman.secretWord;
        int guessesLeft = hangman.guessesLeft;

        //Stavanje na secretWord
        dashBox.setText(secretWord);

        //Kolku se peostanati moznosti ispituvanje vo textView
        //guessLeftBox.setText("Guesses Left: " + hangman.guessesLeft);

        if (bool) {
            if (isUserWon()) {
                if (isSoundOn) {
                    surprised.start();
                }
            } else {
                if (isSoundOn) {
                    rightAnswer.start();
                }
            }
        } else {
            if (isSoundOn) {
                wrongAnswer.start();
            }
            if ((hangman.guessesLeft == 0)) {
                for (int btn : KEYBOARD.keySet()) {
                    Button myButton = (Button) findViewById(btn);
                    myButton.setVisibility(View.INVISIBLE);
                }
                guessLeftBox.setText("The word was: " + Hangman.randomWord);
                //if(Hangman.wordCount>1){
                //Hangman.level-=1;               // <-- Vrakjanje na eden level nazad ako zborot e gresen
                Hangman.wordCount = 0;
                //levelBox.setText("Level: "+Hangman.level);
                // }

                this.numberOfLives -= 1;
                System.out.println("NUMBER OF LIVES " + numberOfLives);
                drawHeart(numberOfLives);


                if ((Hangman.numOfPossibleWords != 0) && (this.numberOfLives != 0)) {
                    View nextWordBtn = findViewById(R.id.nextWord);
                    nextWordBtn.setVisibility(View.VISIBLE);
                    helpLetterBtn.setVisibility(View.INVISIBLE);
                } else {
                    int userId = SharedPrefManager.getInstance(this).getuserId();
                    Intent intent = new Intent(getApplicationContext(), EndGameActivity.class);
                    intent.putExtra("userId", userId);
                    intent.putExtra("userLevel", Hangman.level);
                    intent.putExtra("categoryName", categoryName);
                    startActivity(intent);
                }
            }
        }
    }

    public void changeIconLevel(int level, int guessesLeft) {
        switch (level) {
            case 1:
                level1(guessesLeft);
                break;
            case 2:
                level2(guessesLeft);
                break;
            case 3:
                level3(guessesLeft);
                break;
            case 4:
                level4(guessesLeft);
                break;
            case 5:
                level5(guessesLeft);
                break;
            case 6:
                level6(guessesLeft);
                break;
        }

    }

    public void drawHeart(int numberOfLives) {
        switch (numberOfLives) {
            case 0:
                heart_image.setImageResource(R.drawable.nula);
                break;
            case 1:
                heart_image.setImageResource(R.drawable.edno);
                break;
            case 2:
                heart_image.setImageResource(R.drawable.dve);
                break;
        }
    }

    public static void level1(int guessesLeft) {
        switch (guessesLeft) {
            case 0:
                game_image_view.setImageResource(R.drawable.flingstoncelosno);
                break;
            case 1:
                game_image_view.setImageResource(R.drawable.flingstondesnanoga);
                break;
            case 2:
                game_image_view.setImageResource(R.drawable.flingstondverace);
                break;
            case 3:
                game_image_view.setImageResource(R.drawable.flingstondesnaraka);
                break;
            case 4:
                game_image_view.setImageResource(R.drawable.flingstontorzo);
                break;
            case 5:
                game_image_view.setImageResource(R.drawable.flingstonglava);
                break;
            case 6:
                game_image_view.setImageResource(R.drawable.flingstondrvo);
                break;
        }
    }

    public static void level2(int guessesLeft) {
        switch (guessesLeft) {
            case 0:
                game_image_view.setImageResource(R.drawable.farmercelosno);
                break;
            case 1:
                game_image_view.setImageResource(R.drawable.farmerednanoga);
                break;
            case 2:
                game_image_view.setImageResource(R.drawable.farmerdverace);
                break;
            case 3:
                game_image_view.setImageResource(R.drawable.farmerednaraka);
                break;
            case 4:
                game_image_view.setImageResource(R.drawable.farmertorzo);
                break;
            case 5:
                game_image_view.setImageResource(R.drawable.farmerglava);
                break;
            case 6:
                game_image_view.setImageResource(R.drawable.farmerdrvo);
                break;
        }
    }
    public static void level3(int guessesLeft) {
        switch (guessesLeft) {
            case 0:
                game_image_view.setImageResource(R.drawable.princcelosno);
                break;
            case 1:
                game_image_view.setImageResource(R.drawable.princdesnanoga);
                break;
            case 2:
                game_image_view.setImageResource(R.drawable.princdverace);
                break;
            case 3:
                game_image_view.setImageResource(R.drawable.princdesnaraka);
                break;
            case 4:
                game_image_view.setImageResource(R.drawable.princtorzo);
                break;
            case 5:
                game_image_view.setImageResource(R.drawable.princglava);
                break;
            case 6:
                game_image_view.setImageResource(R.drawable.princdrvo);
                break;
        }
    }
    public static void level4(int guessesLeft) {
        switch (guessesLeft) {
            case 0:
                game_image_view.setImageResource(R.drawable.johnnycelosno);
                break;
            case 1:
                game_image_view.setImageResource(R.drawable.johnnydesnanoga);
                break;
            case 2:
                game_image_view.setImageResource(R.drawable.johnnydverace);
                break;
            case 3:
                game_image_view.setImageResource(R.drawable.johnnydesnaraka);
                break;
            case 4:
                game_image_view.setImageResource(R.drawable.johnnytorzo);
                break;
            case 5:
                game_image_view.setImageResource(R.drawable.johnnyglava);
                break;
            case 6:
                game_image_view.setImageResource(R.drawable.johnnydrvo);
                break;
        }
    }
    public static void level5(int guessesLeft) {
        switch (guessesLeft) {
            case 0:
                game_image_view.setImageResource(R.drawable.robotcelosno);
                break;
            case 1:
                game_image_view.setImageResource(R.drawable.robotdesnanoga);
                break;
            case 2:
                game_image_view.setImageResource(R.drawable.robotdverace);
                break;
            case 3:
                game_image_view.setImageResource(R.drawable.robotdesnaraka);
                break;
            case 4:
                game_image_view.setImageResource(R.drawable.robottorzo);
                break;
            case 5:
                game_image_view.setImageResource(R.drawable.robotglava);
                break;
            case 6:
                game_image_view.setImageResource(R.drawable.farmerdrvo);
                break;
        }
    }
    public static void level6(int guessesLeft) {
        switch (guessesLeft) {
            case 0:
                game_image_view.setImageResource(R.drawable.alien);
                break;
            case 1:
                game_image_view.setImageResource(R.drawable.aliennoga);
                break;
            case 2:
                game_image_view.setImageResource(R.drawable.alienrace);
                break;
            case 3:
                game_image_view.setImageResource(R.drawable.alienraka);
                break;
            case 4:
                game_image_view.setImageResource(R.drawable.alientorzo);
                break;
            case 5:
                game_image_view.setImageResource(R.drawable.alienglava);
                break;
            case 6:
                game_image_view.setImageResource(R.drawable.aliendrvo);
                break;
        }
    }

    public boolean isUserWon() {
        if (hangman.guessesLeft == 0) return false;
        String string = removeSpaces(hangman.secretWord);
        if (hangman.possibleWords.contains(string)) {
            return true;
        } else {
            return false;
        }
    }

    // Stavanje prazno mesto na mestoto kade sto se naogja bukvata vo zborot
    public String removeSpaces(String secretWord) {
        String string = "";
        for (int i = 0; i < secretWord.length(); i++) {
            if (secretWord.charAt(i) != ' ')
                string += secretWord.charAt(i);
        }
        return string;
    }

    public void onClick(View view) {
        int id = view.getId();
        if (KEYBOARD.keySet().contains(id)) {
            //Go naogja kliknatoto kopce po id
            Button button = (Button) findViewById(id);
            //true za da ostane kopceto pusteno i po klikanje
            button.setVisibility(View.INVISIBLE);
            //ja zema vrednosta na izbranata bukva
            char guessedLetter = KEYBOARD.get(id).charValue();
            updateGame(guessedLetter, view);
        }
        if (view.getId() == nextWordBtn.getId()) {   //<-- Za ako e kliknato nextWord
            hangman.guessesLeft = 6;
            hangman.filledFields = 0;
            hangman.getNewWord(dashBox, hangman.randomWord, this, false);
            //guessLeftBox.setText("Tekst"); //Ne raboti so ova --> hangman.guessesLeft
            nextWordBtn.setVisibility(View.INVISIBLE);
            guessLeftBox.setText("");
            if(Hangman.coins>=10) {
                helpLetterBtn.setVisibility(View.VISIBLE);
            }
        }
        if (view.getId() == helpLetterBtn.getId()) {
            hangman.madeHelp(dashBox, this);
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void onBackPressed() {   // Ako go pritisne na back
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing game")
                .setMessage("Are you sure you want to leave game?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        Hangman.coins = 0;
                        Hangman.wordCount = 0;
                        Hangman.level = 1;
                        Hangman.numOfPossibleWords = 4;
                        numberOfLives = 3;
                        startActivity(new Intent(getApplicationContext(), ChooseCategoryActivity.class));
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

}


