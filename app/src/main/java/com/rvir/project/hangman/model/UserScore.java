package com.rvir.project.hangman.model;

public class UserScore {

    private int level;
    private String date;

    public UserScore(int level,String date){
        this.level=level;
        this.date=date;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
