package com.rvir.project.hangman;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private EditText user_name, user_email, user_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        user_name = (EditText) findViewById(R.id.usernameRegistration);
        user_email = (EditText) findViewById(R.id.emailRegistration);
        user_password = (EditText) findViewById(R.id.passwordRegistration);
    }

    public void onRegistration(View view) {

        final String user_nameText = user_name.getText().toString();
        final String user_emailText = user_email.getText().toString();
        final String user_passwordText = user_password.getText().toString();
        /*String type="registration";
        db db = new db(this);                                                          <-- STAROTO
        db.execute(type,user_nameText,user_emailText,user_passwordText);*/

        if (isNetworkConnected()) {
            remoteDB(user_nameText, user_emailText, user_passwordText);
            System.out.println("REMOTE");

        } else {
           Toast.makeText(
                   this,
                   "Pleas check your internet connection",
                   Toast.LENGTH_LONG
           );
        }
    }

    /*public void localDB(String user_nameText, String user_emailText, String user_passwordText) {

        if (user_nameText.equals("") && user_emailText.equals("") && user_passwordText.equals("")) {
            Toast.makeText(
                    this,
                    "Please enter username,email and passowrd",
                    Toast.LENGTH_LONG
            ).show();
        } else if (user_nameText.equals("") && user_emailText.equals("")) {
            Toast.makeText(
                    this,
                    "Please enter username and email",
                    Toast.LENGTH_LONG
            ).show();
        } else if (user_nameText.equals("") && user_passwordText.equals("")) {
            Toast.makeText(
                    this,
                    "Please enter username and password",
                    Toast.LENGTH_LONG
            ).show();
        } else if (user_nameText.equals("")) {
            Toast.makeText(
                    this,
                    "Please enter username ",
                    Toast.LENGTH_LONG
            ).show();
        } else if (user_passwordText.equals("")) {
            Toast.makeText(
                    this,
                    "Please enter passowrd",
                    Toast.LENGTH_LONG
            ).show();
        } else if (user_emailText.equals("")) {
            Toast.makeText(
                    this,
                    "Please enter passowrd",
                    Toast.LENGTH_LONG
            ).show();
        } else {
            List<User> checkUsername = DataBase.getInstance(getApplicationContext()).daoAccess().checkIfUsernameExists(user_nameText);
            List<User> checkEmail = DataBase.getInstance(getApplicationContext()).daoAccess().checkIfEmailExists(user_emailText);
            if ((checkUsername.size() > 0) && (checkEmail.size() > 0)) {
                Toast.makeText(
                        this,
                        "Username and password already exists",
                        Toast.LENGTH_LONG
                ).show();
            } else if ((checkUsername.size() > 0) && (checkEmail.size() == 0)) {
                Toast.makeText(
                        this,
                        "Username already exist",
                        Toast.LENGTH_LONG
                ).show();
            } else if ((checkUsername.size() == 0) && (checkEmail.size() > 0)) {
                Toast.makeText(
                        this,
                        "Password already exist",
                        Toast.LENGTH_LONG
                ).show();
            } else {
                int userId = DataBase.getInstance(getApplicationContext()).daoAccess().getLastInserted();
                SharedPrefManager.getInstance(getApplicationContext()).userLogin(userId, user_nameText, user_emailText);
                DataBase.getInstance(getApplicationContext()).daoAccess().register(new User(user_nameText, user_emailText, user_passwordText));
                Intent intent = new Intent(getApplicationContext(), ChooseCategoryActivity.class);
                startActivity(intent);
            }
        }
    }*/

    public void remoteDB(String user_nameText, String user_emailText, String user_passwordText) {

        final String nameText = user_name.getText().toString();
        final String emailText = user_email.getText().toString();
        final String passwordText = user_password.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            System.out.println("TUKAAAAAA " + response);
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray re = jsonObject.getJSONArray("result");

                            Boolean error = false;

                            for (int i = 0; i < re.length(); i++) {
                                JSONObject jo = re.getJSONObject(i);
                                error = jo.getBoolean("error");
                                if (error == true) {
                                    String message = jo.getString("msg");
                                    Toast.makeText(getApplicationContext(), message,
                                            Toast.LENGTH_LONG).show();
                                } else if (error == false) {
                                    String userName = jo.getString("name");
                                    String userEmail = jo.getString("email");
                                    int userId = jo.getInt("id");
                                    Intent intent = new Intent(getApplicationContext(), ChooseCategoryActivity.class);
                                    startActivity(intent);
                                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(userId, userName, userEmail);
                                }
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_name", nameText);
                params.put("email", emailText);
                params.put("password", passwordText);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void onLogin(View view){
        Intent intent = new Intent(getApplicationContext(), login.class);
        startActivity(intent);
    }


}