package com.rvir.project.hangman;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by project on 18/11/16.
 */

public class Hangman {

    //Se kreira lista so mozni zborcinja
    public TreeSet<String> possibleWords = new TreeSet<String>();
    private GameplayActivity gameplayActivity;
    public String secretWord;
   //public int wordLength;
    public int guessesLeft;
    public int filledFields;
    public static String randomWord;
    public Boolean checkNewWord;
    private HashMap<String, Integer> contains;
    public static int level = 1;  // Za promena na levelite
    public static int coins = 0; // Za sobiranje na poetnite
    public static int wordCount = 0; // Za sobiranje na tocnite zborcinja
    public static int numOfPossibleWords = 12;// 5 numOfPossibleWords za 4 zbora

    //Stavame ArrayLista od zborcinja ,dolzina na zborot i guesses left ,vrz osnova na izbranata golemina na zborot od
    //arraylistata izbira zborovi i gi dodava vo possibleWords
    public Hangman(ArrayList<String> wordset, int guessesLeft) {
        for (String word : wordset) {
                possibleWords.add(word);
        }
        this.filledFields = 0;
        randomWord = getRandomElement(possibleWords);//Ova go dodadov za da zema random zborce
        //this.wordLength = randomWord.length();
        secretWord = getSecretWord(randomWord.length());// Za crtickite sto se ispisuvaat
        //this.checkNewWord = false;
        //this.wordLength =wordLength;
        this.contains = new HashMap<>();
        contains = wordContains(randomWord);
        this.guessesLeft = 6;
    }


    //this function generates the word family the word belongs to
    //Koga ke bide pogodena vnesenata bukva dolnata crta ja zamenuva so vnesenata bukva
    public String getPattern(String word, char guessedLetter) {
        String regExp = "[^" +guessedLetter+ "]";
        //Tuka go smeniv (?i)"+regExp).trim() posto ako bese prvata bukva na zborot golema a tocno si izbral ja racunase gresno
        return word.replaceAll(("(?i)" + regExp).trim(), "_");
    }

    public TreeMap<String, TreeSet<String>> partitionIntoWordFamily(char guessedLetter) {
        TreeMap<String, TreeSet<String>> wordfamily = new TreeMap<>();
        TreeSet<String> currentSet = new TreeSet<String>();
        String pattern = getPattern(randomWord, guessedLetter);
        currentSet.add(randomWord); //Site zborcinja od possible words gi dodava tuka
        wordfamily.put(pattern, currentSet);
        return wordfamily;
    }

    public void madeHelp(TextView dashBox, Context context) {
        char letter;
        coins -= 10;
        GameplayActivity.coinsBox.setText(": " + coins);
        for (String name : this.contains.keySet()) {
            int value = this.contains.get(name);
            if (value > 0) {
                letter = name.charAt(0);
                contains.put(name, contains.get(name) - value);
                this.filledFields = this.filledFields + value;
                boolean bool = guessMade(letter, dashBox, context);
                if (bool) {
                    dashBox.setText(secretWord);
                }
                break;
            }
        }
        GameplayActivity.helpLetterBtn.setVisibility(View.INVISIBLE);
    }

    //Ja proveruva vnesenata bukva dali se sodrzi vo zborot
    public boolean guessMade(char guess, TextView dashBox, Context context) {
        TreeMap<String, TreeSet<String>> wordfamily = partitionIntoWordFamily(guess);
        int maxLength = 0;
        String key = "";
        for (String string : wordfamily.keySet()) {
            key = string;
        }
        secretWord = getSecretWord(key);
        System.out.println("Secret Word "+secretWord);
        for (String name : this.contains.keySet()) {
            int value = this.contains.get(name);
            if (String.valueOf(guess).equals(name)) {
                if (value > 0) {
                    contains.put(name, contains.get(name) - value);
                    this.filledFields = this.filledFields + value;
                }
            }
        }
        if (!secretWord.toUpperCase().contains(("" + guess).toUpperCase())) {
            guessesLeft = guessesLeft - 1;
            if (filledFields == randomWord.length()) {
                this.filledFields = 0;
                getNewWord(dashBox, this.randomWord, context, true);
            }
            return false;
        } else {
            if (filledFields == randomWord.length()) {
                MediaPlayer applause = MediaPlayer.create(context, R.raw.applause);
                applause.start();
                this.filledFields = 0;
                getNewWord(dashBox, this.randomWord, context, true);
            }
            return true;
        }
    }

    public void getNewWord(TextView dashbox, String word, Context context, Boolean tocen) {

        possibleWords.remove(word); //za da ne go zeme povtorno istiot zbor
        this.numOfPossibleWords -= 1;//Gi namaluva zborovite za taa igra
        if ((numOfPossibleWords != 0)) {
            if ((tocen == true)) {  //Ako zborceto e tocno dodava za pogoden zbor wordCount+1
                this.wordCount += 1;
                if (this.wordCount == 2) {  // <-- Zgolemuvanje na level ako ima 2 pogodeno zbora
                    this.level += 1;
                    GameplayActivity.levelBox.setText(" : " + level); // da se prikaze promenetiot level vo textView
                    this.wordCount = 0; //wordCount povtorno se naznaviva na nula za sledniot level
                }
                coins += 5; //Za pogoden zbor se sobira +5 coins
                GameplayActivity.coinsBox.setText(": " + coins); //se prikazuvaat coins
                if (coins >= 10) {
                    GameplayActivity.helpLetterBtn.setVisibility(View.VISIBLE);   //Ako ima povise od 10 coins da kupi bukva
                }
            }
            if (this.possibleWords.size() != 0) {
                this.randomWord = getRandomElement(this.possibleWords);
                this.contains = wordContains(this.randomWord);
                this.secretWord = getSecretWord(randomWord.length());
                this.guessesLeft = 6;
                if(tocen==false) {
                    gameplayActivity.level1(guessesLeft);
                }

                visibleButtons();
                dashbox.setText(getSecretWord(randomWord.length()));

            } else {
                System.out.println("TOLKU ZBORCINJA IMA VO BAZATA.TREBA DA DODADEME NOVI"); // <-- Dodeka ne stavime novi da se ispisuva ova vo console
                                                                                            // dokolku snema zborovi dodeka trae igrata.
            }
        } else {
            int userId = SharedPrefManager.getInstance(context).getuserId();
            System.out.println("USER ID "+userId);
            System.out.println("USER LEVEL "+ Hangman.level);
            System.out.println("USER CATEGORY "+GameplayActivity.categoryName);
            Intent intent = new Intent(context, EndGameActivity.class);  // <-- Koga ke napravam layout za taa aktivnost
            intent.putExtra("userId", userId);
            intent.putExtra("userLevel", Hangman.level);
            intent.putExtra("categoryName", GameplayActivity.categoryName);
            context.startActivity(intent);
        }
    }

    //Crticki sto ke se ispisat vo dashbox vo zavisnost od goleminata od zborot
    //Metodata se upotrebuva vo GameActivity i vo konstruktorot vo ovaa klasa
    public String getSecretWord(int letters) {
        String dashes = "";
        for (int i = 0; i < letters - 1; i++) {
            dashes += "_ ";
        }
        dashes += "_";
        return dashes;
    }

    public String getRandomElement(TreeSet<String> list) {
        int size = list.size();
        //System.out.println("Size "+size);
        if (!list.isEmpty()) {
            int item = new Random().nextInt(size);
            int i = 0;
            String random = null;
            for (String string : list) {
                if (i == item)
                    random = string;
                i++;
            }
            return random;
        }
        return null;
    }

    //Metodata se upotrebuva samo vo ovaa klasa
    //Koga bukvata ke bide pogodena ja zamenuva na crtickite
    public String getSecretWord(String string) {
        //System.out.println("Secret word vo getSecretWord " + secretWord + " String " + string);
        String dashes = "";
        int index;
        for (int i = 0; i < string.length() - 1; i++) {
            if (i == 0)
                index = i;
            else
                index = 2 * i;
            if (secretWord.charAt(index) != '_') {
                dashes += secretWord.charAt(index) + " ";
            } else {
                dashes += string.charAt(i) + " ";
            }
        }
        index = 2 * (string.length() - 1);
        if (secretWord.charAt(index) != '_') {
            dashes += secretWord.charAt(index);
        } else {
            dashes += string.charAt(string.length() - 1);
        }
        return dashes;
    }

    public HashMap<String, Integer> wordContains(String word) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        String[] letters = word.split(" ");
        for (String letter : letters) {
            letter = letter.toLowerCase();
            for (int i = 0; i < letter.length(); i++) {
                char c = letter.charAt(i);
                if (hashMap.containsKey(c + "")) {
                    int count = hashMap.get(c + "");
                    hashMap.put(c + "", count + 1);
                } else
                    hashMap.put(c + "", 1);
            }
        }
        return hashMap;
    }

    public void visibleButtons() {
        GameplayActivity.button1.setVisibility(View.VISIBLE);
        GameplayActivity.button2.setVisibility(View.VISIBLE);
        GameplayActivity.button3.setVisibility(View.VISIBLE);
        GameplayActivity.button4.setVisibility(View.VISIBLE);
        GameplayActivity.button5.setVisibility(View.VISIBLE);
        GameplayActivity.button6.setVisibility(View.VISIBLE);
        GameplayActivity.button7.setVisibility(View.VISIBLE);
        GameplayActivity.button8.setVisibility(View.VISIBLE);
        GameplayActivity.button9.setVisibility(View.VISIBLE);
        GameplayActivity.button10.setVisibility(View.VISIBLE);
        GameplayActivity.button11.setVisibility(View.VISIBLE);
        GameplayActivity.button12.setVisibility(View.VISIBLE);
        GameplayActivity.button13.setVisibility(View.VISIBLE);
        GameplayActivity.button14.setVisibility(View.VISIBLE);
        GameplayActivity.button15.setVisibility(View.VISIBLE);
        GameplayActivity.button16.setVisibility(View.VISIBLE);
        GameplayActivity.button17.setVisibility(View.VISIBLE);
        GameplayActivity.button18.setVisibility(View.VISIBLE);
        GameplayActivity.button19.setVisibility(View.VISIBLE);
        GameplayActivity.button20.setVisibility(View.VISIBLE);
        GameplayActivity.button21.setVisibility(View.VISIBLE);
        GameplayActivity.button22.setVisibility(View.VISIBLE);
        GameplayActivity.button23.setVisibility(View.VISIBLE);
        GameplayActivity.button24.setVisibility(View.VISIBLE);
        GameplayActivity.button25.setVisibility(View.VISIBLE);
        GameplayActivity.button26.setVisibility(View.VISIBLE);
    }


}
