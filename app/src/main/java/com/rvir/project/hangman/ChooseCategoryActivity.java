package com.rvir.project.hangman;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ChooseCategoryActivity extends AppCompatActivity {

    GridView gridView;
    String[] categories = {"Fruits","Vegetables","Drinks", "Sports", "Countries", "Animals","Cities","Cars"};
    int[] categoriesImages = {R.drawable.fruits, R.drawable.vegetables, R.drawable.drinks, R.drawable.sports, R.drawable.countries,
            R.drawable.animals,R.drawable.cities, R.drawable.cars};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        //finding listview
        gridView = (GridView) findViewById(R.id.gridview);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), GameplayActivity.class);
                intent.putExtra("name", categories[i]);
                intent.putExtra("image", categoriesImages[i]);
                startActivity(intent);

            }
        });
    }

    // Kreiranje na menito
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gameplay, menu);
        return true;
    }

    //Ako e kliknato to za google sto da se otvori (Tuka da promenam za setings namesto google)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_HowToPlay) {
            Intent intent = new Intent(getApplicationContext(),InstructionActivity.class);
            startActivity(intent);
            return true;
        } else if(item.getItemId() == R.id.action_logOut){
            SharedPrefManager.getInstance(this).logout();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.action_HighScore){
            Intent intent = new Intent(getApplicationContext(), HighScoreActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return categoriesImages.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data, null);
            TextView name = (TextView) view1.findViewById(R.id.fruits);
            ImageView image = (ImageView) view1.findViewById(R.id.images);

            name.setText(categories[i]);
            image.setImageResource(categoriesImages[i]);
            return view1;

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
}
