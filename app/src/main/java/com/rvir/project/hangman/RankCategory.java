package com.rvir.project.hangman;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rvir.project.hangman.model.UserRank;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RankCategory extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.action_back);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_rank);

        Intent intent = getIntent();
        String categoryName = intent.getStringExtra("categoryName");

        userRank(this,categoryName);


        //System.out.println("Category Name vo RANK CATEOGRY " + categoryName);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.action_back) {
                    Intent intentRank = new Intent(getApplicationContext(), RankFragment.class);
                    startActivity(intentRank);
                }
                return true;
            }
        });

    }

    public void userRank(Context context,String categoryName){
    final String categoryNameInput = categoryName;
    final Context context1=context;
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_HighScore,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<UserRank> userRanks = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArrayResult = jsonObject.getJSONArray("result");
                            JSONArray jsonArrayRanks = jsonObject.getJSONArray("ranks");
                            JSONObject jo = jsonArrayResult.getJSONObject(0);
                            if (!jo.getBoolean("error")) {
                                for (int i = 0; i < jsonArrayRanks.length(); i++) {
                                    System.out.println("Rank TUKAAAAAA " + jsonArrayRanks.getJSONObject(i).getString("userName")+
                                            "Level TUKAAAAA"+jsonArrayRanks.getJSONObject(i).getString("level"));
                                    userRanks.add(
                                            new UserRank(jsonArrayRanks.getJSONObject(i).getString("userName"),
                                                    jsonArrayRanks.getJSONObject(i).getString("level")));
                                }
                            } else {
                                jo = jsonArrayResult.getJSONObject(1);
                                System.out.println("TUKAA ERROR");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        AllUserScoresAdapter allUserScoresAdapter = new AllUserScoresAdapter(context1,userRanks);
                        recyclerView.setAdapter(allUserScoresAdapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(context1));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("category_name", categoryNameInput);
                return params;
            }
        };
        RequestHandler.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

    }

}


