package com.rvir.project.hangman;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

import com.rvir.project.hangman.model.UserRank;

import java.util.ArrayList;

public class AllUserScoresAdapter  extends RecyclerView.Adapter<AllUserScoresAdapter.ViewHolder> {

    Context context;
    ArrayList<UserRank> userRanks;

    public AllUserScoresAdapter(Context context,ArrayList<UserRank> userRanks){
        this.context=context;
        this.userRanks=userRanks;
    }

    @NonNull
    @Override
    public AllUserScoresAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.rank_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (i == 0) {
            viewHolder.medal.setImageResource(R.drawable.medalfirst);
        } else if (i == 1) {
            viewHolder.medal.setImageResource(R.drawable.medalsecond);
        } else if (i == 2) {
            viewHolder.medal.setImageResource(R.drawable.medalthird);
        } else{
            viewHolder.medal.setImageResource(R.color.whitecolor);
        }
        viewHolder.textView.setText(String.valueOf(i+1));
        viewHolder.textView1.setText(userRanks.get(i).getLevel());
        viewHolder.textView2.setText(userRanks.get(i).getUserName());

    }

    @Override
    public int getItemCount() {
        return userRanks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        TextView textView1;
        TextView textView2;
        ImageView medal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.rank);
            textView1=itemView.findViewById(R.id.userlevelTextView);
            textView2=itemView.findViewById(R.id.userTextView);
            medal = itemView.findViewById(R.id.medal);
        }
    }


}
