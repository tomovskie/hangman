package com.rvir.project.hangman;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rvir.project.hangman.model.Game;
import com.rvir.project.hangman.model.UserScore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import localDB.DataBase;

public class EndGameActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endgame);

        final Context context = this;

        recyclerView = findViewById(R.id.my_recycler_view_user_scores);

        bottomNavigationView = findViewById(R.id.navigation);

        Intent intent = getIntent();
        int userid = intent.getIntExtra("userId", 0);
        int userLevel = intent.getIntExtra("userLevel", 0);
        String categoryName = intent.getStringExtra("categoryName");

        /*System.out.println("VO EndGameActivity");
        System.out.println("USER ID " + userid);
        System.out.println("USER LEVEL " + userLevel);
        System.out.println("CATEGORY NAME " + categoryName);*/

        boolean bool = isConnected(this);

        if (bool) {
            saveResults(userid, categoryName, userLevel);  //  Ako ima konekcija so internet rezultatite se zacuvuvaat vo online bazata
            lastResults(userid,this);
        } else {
            saveResultsLocalDB(userid, categoryName, userLevel); // Ako nema konekcija rezultatite se zacuvuvaat vo lokalnata baza
            lastResultsLocalDB(userid);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case  R.id.action_rank:
                        //selected = RankFragment.newInstance();
                        if (isConnected(getApplicationContext())) {
                            Intent intentRank = new Intent(getApplicationContext(), RankFragment.class);
                            startActivity(intentRank);
                        }else{
                            System.out.println("TUKAAAAAAAAAAAAAAAAAAAAAAAa AAAAAAAAAAAAAA");
                            Toast.makeText(
                                    context,
                                    "Please check your internet connection",
                                    Toast.LENGTH_LONG);
                        }
                        break;
                    case  R.id.action_newGame:
                        Hangman.coins = 0;
                        Hangman.wordCount = 0;
                        Hangman.level = 1;
                        Hangman.numOfPossibleWords = 12;
                        GameplayActivity.numberOfLives = 3;
                        Intent intent = new Intent(getApplicationContext(), ChooseCategoryActivity.class);
                        startActivity(intent);
                        break;
                }
                return  true;
            }
        });

        //HighScoreAdapter highScoreAdapter = new HighScoreAdapter(this,);

    }

    public void saveResults(int userId, String categoryName, int userLevel) {
        final String userIdInput = String.valueOf(userId); //mora so string deka dole vo metodata ne dava return type na parametri integer da bide
        final String categorynameInput = categoryName;
        final String userLevelInput = String.valueOf(userLevel);
        final String dateTime = getDate();
        DataBase.getInstance(this).daoAccess().addEndGameResult(new Game(
                userLevel,
                categoryName,
                userId,
                1,
                dateTime
        ));
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_SaveScore,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            Boolean error = false;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObjectArray = jsonArray.getJSONObject(i);
                                error = jsonObjectArray.getBoolean("error");
                                if (error = true) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "Results stored",
                                            Toast.LENGTH_LONG);
                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "Error",
                                            Toast.LENGTH_LONG);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userIdInput);
                params.put("category_name", categorynameInput);
                params.put("user_level", userLevelInput);
                return params;
            }
        };
        RequestHandler.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void saveResultsLocalDB(int userId, String categoryName, int userLevel) {
        String dateTime = getDate();
        DataBase.getInstance(this).daoAccess().addEndGameResult(new Game(
                userLevel,
                categoryName,
                userId,
                0,
                dateTime
        ));
    }

    public String getDate() {
        StringBuffer stringBuffer = new StringBuffer();
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.format(now, stringBuffer, new FieldPosition(0));
        String dateTime = simpleDateFormat.format(new Date());
        return dateTime;
    }


    @Override
    public void onBackPressed() {   // Ako klikne na back da go vrati na chooseCategoryActivity
        super.onBackPressed();
        Hangman.coins = 0;
        Hangman.wordCount = 0;
        Hangman.level = 1;
        Hangman.numOfPossibleWords = 12;
        GameplayActivity.numberOfLives = 3;
        startActivity(new Intent(this, ChooseCategoryActivity.class));
    }

    public static boolean isConnected(Context context) {   //Ovaa metoda funkcionira tuka za isConnected
        ConnectivityManager
                cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    public void lastResults(int userId, final Context context) {
        final String userIdInput = String.valueOf(userId); //mora so string deka dole vo metodata ne dava return type na parametri integer da bide
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_UserResults,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<UserScore> userScores = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObjectArray = jsonArray.getJSONObject(i);
                                userScores.add(new UserScore(jsonObjectArray.getInt("level"),jsonObjectArray.getString("date")));
                                //System.out.println("Level TUKAA "+jsonObjectArray.getInt("level")+" date "+jsonObjectArray.getString("date"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        HighScoreAdapter highScoreAdapter = new HighScoreAdapter(context,userScores);
                        recyclerView.setAdapter(highScoreAdapter);
                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userIdInput);
                return params;
            }
        };
        RequestHandler.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void lastResultsLocalDB(int userId) {
        System.out.println("Lokalno vo endGameActivity");
        ArrayList<Game> userGames = (ArrayList<Game>) DataBase.getInstance(this).daoAccess().getGetUserResults(userId);
        ArrayList<UserScore> userScores = new ArrayList<>();
        for (Game game:userGames
             ) {
            userScores.add(new UserScore(game.getLevel(),game.getDate()));
        }
        HighScoreAdapter highScoreAdapter = new HighScoreAdapter(this,userScores);
        recyclerView.setAdapter(highScoreAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


}
