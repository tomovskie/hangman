package com.rvir.project.hangman.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class User {

    @PrimaryKey
    private int id;
    private String name;
    //private String email;
    //private String password;
    //private int  isSynced; // Proveruva dali e sinhronizirana

    @Ignore
    public User(){

    }

    public User(int id,String name){
        this.id = id;
        this.name=name;
    }

    /*@Ignore
    public User(int id,String name,String email,String password){
        this.id = id;
        this.name=name;
        this.email=email;
        this.password=password;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

 /*   public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    */
}
