package com.rvir.project.hangman;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/* Ovaa klasa ne se koristi  */
public class ServerConnection {

    static HttpURLConnection httpURLConnection = null;

    private ServerConnection(){

    }

    public static HttpURLConnection getHttpURLConnection(){
        String login_url = "http://hm-r-v-i-r.000webhostapp.com/login.php";
        if(httpURLConnection==null)
        {
            try {
                URL url = new URL(login_url);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                return  httpURLConnection;
        }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return httpURLConnection;
    }

}
