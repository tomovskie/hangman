package com.rvir.project.hangman.model;

import androidx.room.Embedded;

public class GameWithUser {

    @Embedded
    User user;

    @Embedded
    Game game;

}
